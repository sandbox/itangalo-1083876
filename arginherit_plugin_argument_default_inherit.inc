<?php
/**
 * @file
 * Contains the validator to replace a nid with nids for referenced nodes.
 */

$path = drupal_get_path('module', 'views') . '/plugins/views_plugin_argument_default.inc';
require_once($path);

/**
 * Defines a default argument that inherits value from another argument.
 */
class arginherit_plugin_argument_default_inherit extends views_plugin_argument_default {
  var $option_name = 'argument_number';

  // Build the form for this default argument.
  function argument_form(&$form, &$form_state) {
    $form[$this->option_name] = array(
      '#type' => 'textfield',
      '#size' => 1,
      '#maxlength' => 1,
      '#title' => t('The argument to inherit'),
      '#default_value' => $this->argument->options[$this->option_name],
      '#process' => array('views_process_dependency'),
      '#description' => t('Enter the number of the argument to inherit value from.'),
      '#dependency' => array(
        'radio:options[default_argument_type]' => array($this->id)
      ),
    );
  }

  // Set the argument.
  function get_argument($raw = FALSE) {

    // Get all the arguments for this view.
    $all_arguments = &$this->view->args;
    // Get the argument to inherit. Note that this is shifted one step, in order
    // to compensate for the fact that Views interface starts the numberin on
    // one while the index for the arguments starts on zero.
    $argument_to_inherit = $this->argument->options[$this->option_name] - 1;

    return $all_arguments[$argument_to_inherit];
  }
}
