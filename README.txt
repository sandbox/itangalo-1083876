This module provides a new default handler for Views, allowing the argument to
inherit the value from another argument.

This is probably not extremely useful in most cases, but in some cases it is
kind of important that two different arguments have the same input values. This
could for example be the case if you're using DraggableViews and Node References
in Views together, and want to have a null argument with a parent nid and one
argument with all the referenced nodes.

I don't know enough about Views coding to be sure that I've done everything
right, and I suspect that 'require_once' at the top of
arginherit_plugin_argument_default_inherit.inc should be handled in another way.
Feel free to tell me how to do it!


PS: I *did* search quite a bit for existing modules doing this, but didn't find
any. I might be wrong, but in that case the module is hiding pretty well.