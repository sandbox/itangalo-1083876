<?php
/**
 * @file
 * Declares the Views plugin for inheriting argument value.
 */

function arginherit_views_plugins() {
  return array(
    'argument default' => array(
      'inherit' => array(
        'title' => t('Inherit argument'),
        'handler' => 'arginherit_plugin_argument_default_inherit',
      ),
    ),
  );
}
